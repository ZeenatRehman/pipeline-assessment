package com.relay42.pipeline.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.relay42.pipeline.model.IOTReading;
import com.relay42.pipeline.model.IOTTypeEnum;
import com.relay42.pipeline.model.request.IOTSummaryRequest;
import com.relay42.pipeline.model.response.IOTSummaryResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class IOTServiceTest
{
    List<IOTReading> list;
    @Autowired
    IOTService service;

    @BeforeEach
    void setUp() throws IOException
    {
        list = new IOTReadingListBuilder().build();
        list.forEach(e -> service.saveIOTDate(e));


    }

    @AfterEach
    void tearDown()
    {
        service.deleteAll();
    }

    @Test
    void findAggregationFor2Sec()
    {
        IOTSummaryRequest request = new IOTSummaryRequestBuilder()
                .build("2021-03-26 09:39:38", "2021-03-26 09:39:39");
        List<IOTSummaryResponse> result = service.findAggregation(request);
        assertAll(
                () -> assertEquals(19.31079036192166, result.get(0).getMedian()),
                () -> assertEquals("CAR_FUEL_READER", result.get(0).getDeviceType()),
                () -> assertEquals(17.508563885851295, result.get(0).getAverage()),
                () -> assertEquals(25.090639003158454, result.get(0).getMax()),
                () -> assertEquals(6.322035816403416, result.get(0).getMin()),
                () -> assertEquals(4, result.get(0).getCount()),

                () -> assertEquals(32.65258164625671, result.get(1).getMedian()),
                () -> assertEquals("HEART_RATE_MONITOR", result.get(1).getDeviceType()),
                () -> assertEquals(37.06062606415881, result.get(1).getAverage()),
                () -> assertEquals(76.38978189731046, result.get(1).getMax()),
                () -> assertEquals(6.54755906681137, result.get(1).getMin()),
                () -> assertEquals(4, result.get(1).getCount()),

                () -> assertEquals(70.23625979947089, result.get(2).getMedian()),
                () -> assertEquals("THERMOSTAT", result.get(2).getDeviceType()),
                () -> assertEquals(60.868243001728345, result.get(2).getAverage()),
                () -> assertEquals(91.10792741887734, result.get(2).getMax()),
                () -> assertEquals(11.892524989094266, result.get(2).getMin()),
                () -> assertEquals(4, result.get(2).getCount())
        );
    }


    @Test
    void findAggregationFor1Sec()
    {
        IOTSummaryRequest request = new IOTSummaryRequestBuilder()
                .build("2021-03-26 09:39:38", "2021-03-26 09:39:38");
        List<IOTSummaryResponse> result = service.findAggregation(request);
        assertAll(
                () -> assertEquals(12.180539959754999, result.get(0).getMedian()),
                () -> assertEquals("CAR_FUEL_READER", result.get(0).getDeviceType()),
                () -> assertEquals(12.180539959754999, result.get(0).getAverage()),
                () -> assertEquals(18.039044103106583, result.get(0).getMax()),
                () -> assertEquals(6.322035816403416, result.get(0).getMin()),
                () -> assertEquals(2, result.get(0).getCount()),

                () -> assertEquals(32.65258164625671, result.get(1).getMedian()),
                () -> assertEquals("HEART_RATE_MONITOR", result.get(1).getDeviceType()),
                () -> assertEquals(32.65258164625671, result.get(1).getAverage()),
                () -> assertEquals(48.97726226833359, result.get(1).getMax()),
                () -> assertEquals(16.327901024179823, result.get(1).getMin()),
                () -> assertEquals(2, result.get(1).getCount()),

                () -> assertEquals(72.41018215285555, result.get(2).getMedian()),
                () -> assertEquals("THERMOSTAT", result.get(2).getDeviceType()),
                () -> assertEquals(72.41018215285555, result.get(2).getAverage()),
                () -> assertEquals(91.10792741887734, result.get(2).getMax()),
                () -> assertEquals(53.71243688683378, result.get(2).getMin()),
                () -> assertEquals(2, result.get(2).getCount())
        );
    }
}

class IOTReadingListBuilder
{
    public List<IOTReading> build() throws IOException
    {
        List<IOTReading> list = new ArrayList<>();
        File file = new File("src/test/resources/ReadingsFile.txt");
        try (InputStream fis = new FileInputStream(file);
             InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)
        ) {
            String data;
            while ((data = br.readLine()) != null) {
                ObjectMapper mapper = new ObjectMapper();
                IOTReading reading = mapper.readValue(data, IOTReading.class);
                list.add(reading);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return list;
    }
}

class IOTSummaryRequestBuilder
{
    IOTSummaryRequest build(String startTimeStr, String endTimeStr)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startDateTime = LocalDateTime.parse(startTimeStr, formatter);
        LocalDateTime endDateTime = LocalDateTime.parse(endTimeStr, formatter);

        return  new IOTSummaryRequest(startDateTime,
                endDateTime,
                Arrays.asList(IOTTypeEnum.CAR_FUEL_READER,
                        IOTTypeEnum.HEART_RATE_MONITOR,
                        IOTTypeEnum.THERMOSTAT));

    }
}