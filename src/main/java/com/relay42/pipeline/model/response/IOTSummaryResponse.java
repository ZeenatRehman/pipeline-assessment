package com.relay42.pipeline.model.response;

import com.relay42.pipeline.model.IOTTypeEnum;

/**
 * Class that depicts the modal of the response for /iot/summary rest service.
 */
public class IOTSummaryResponse {
    private double average;
    private double max;
    private double min;
    private double median;
    private double count;
    private String deviceType;

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMedian() {
        return median;
    }

    public void setMedian(double median) {

        this.median = median;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public String getDeviceType() {
        return deviceType;
    }

    @Override
    public String toString()
    {
        return "IOTSummaryResponse{" +
                "average=" + average +
                ", max=" + max +
                ", min=" + min +
                ", median=" + median +
                ", count=" + count +
                ", deviceType='" + deviceType + '\'' +
                '}';
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
