package com.relay42.pipeline.model;

/**
 * defines the units of IOT device's reading. Please add a new unit when you add a new type in
 * IOTTypeEnum
 */
public enum IOTUnitEnum {
    BPM, KMPL, C
}
