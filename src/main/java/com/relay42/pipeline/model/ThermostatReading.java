package com.relay42.pipeline.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.relay42.pipeline.simulation.IOTDataSimulationFactory;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

/**
 * Class that depicts the modal of a specific kind of IOT device
 */
@JsonPropertyOrder({ "id", "deviceType", "readingValue", "readingUnit","readingDateTime"})
@JsonIgnoreProperties(ignoreUnknown=true)
@Document(value = "DATA_IOT_DEVICES")
public class ThermostatReading extends IOTReading {
    public ThermostatReading(){
        initialize();
    }
    public ThermostatReading(String deviceID){
        initialize();
        this.deviceId=deviceID;
    }
    private void initialize() {
        this.deviceType = IOTTypeEnum.THERMOSTAT;
        this.readingUnit = IOTUnitEnum.C;
    }
}
