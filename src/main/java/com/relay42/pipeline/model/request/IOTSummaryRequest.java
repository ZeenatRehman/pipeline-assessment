package com.relay42.pipeline.model.request;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.relay42.pipeline.model.IOTTypeEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that depicts the model of the post request for /iot/summary rest service.
 */
@JsonPropertyOrder({ "startDateTime", "endDateTime", "types"})
public class IOTSummaryRequest {
    public IOTSummaryRequest(){

    }
    public IOTSummaryRequest(LocalDateTime startDateTime, LocalDateTime endDateTime, List<IOTTypeEnum> types){
      this.deviceTypes = types;
      this.startDateTime = startDateTime;
      this.endDateTime = endDateTime;
    }

    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private List<IOTTypeEnum> deviceTypes;

    public LocalDateTime getStartDateTime() {
        if(startDateTime == null) return LocalDateTime.MIN;
        else
            return startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        if(endDateTime == null ) return LocalDateTime.MAX;
        else
            return endDateTime;
    }

    public List<IOTTypeEnum> getDeviceTypes() {
        if(deviceTypes == null)
            return new ArrayList<>();
        else
            return deviceTypes;
    }
}
