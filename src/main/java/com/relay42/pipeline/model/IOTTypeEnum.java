package com.relay42.pipeline.model;

/**
 * defines the types of IOT devices the system supports right now.
 * Add a new type to start using a new IOT device.
 */
public enum IOTTypeEnum {
    CAR_FUEL_READER, HEART_RATE_MONITOR, THERMOSTAT
}
