package com.relay42.pipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import com.relay42.pipeline.utility.IOTDocumentUtil;

/**
 * Main application class
 */
@EnableSwagger2
@SpringBootApplication
public class PipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(PipelineApplication.class, args);
	}
	@Bean
	public Docket configProductsDocket()
	{
		return IOTDocumentUtil.getSessionsDocket();
	}
}
