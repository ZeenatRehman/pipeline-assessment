package com.relay42.pipeline.service;

import com.relay42.pipeline.model.IOTReading;
import com.relay42.pipeline.model.request.IOTSummaryRequest;
import com.relay42.pipeline.model.response.IOTSummaryResponse;
import com.relay42.pipeline.repository.IOTRepository;
import com.relay42.pipeline.utility.IOTSummaryHelper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.stereotype.Service;


import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Service layer implementation. It routes calls from controller to repository
 * and other utilities
 */
@Service
public class IOTService
{
    private static final Logger logger = Logger.getLogger(IOTService.class);
    private final IOTRepository repository;
    private final MongoTemplate mongoTemplate;
    private final IOTSummaryHelper summaryUtility;

    @Autowired
    public IOTService(MongoTemplate mongoTemplate,
                      IOTRepository repository,
                      IOTSummaryHelper summaryUtility)
    {
        this.mongoTemplate = mongoTemplate;
        this.repository = repository;
        this.summaryUtility = summaryUtility;
    }


    /**
     * Save the new IOT data document in collection
     *
     * @param IoTReading new data document.
     */
    public void saveIOTDate(IOTReading IoTReading)
    {
        repository.save(IoTReading);
    }

    /**
     * Method that gets called from post service and returns all aggregate summary
     * stats based on the passed request object.
     *
     * @param isr the request object which has filters for startDate, endDate and deviceTypes
     * @return Summary list with all the aggregate metrics for asked deviceTypes
     */
    public List<IOTSummaryResponse> findAggregation(IOTSummaryRequest isr)
    {
        logger.debug("Service findAggregation started");
        List<IOTSummaryResponse> result = findNonMedianAggregation(isr);
        result = findMedianAggregation(isr, result);
        logger.debug("Service findAggregation result " + result);
        return result;
    }

    private List<IOTSummaryResponse> findMedianAggregation(
                          IOTSummaryRequest isr,
                          List<IOTSummaryResponse> result)
    {
        if (result.size() > 0) {
            List<IOTReading> sortedList = getSortedDBResultForMedianCalc(isr);
            result = summaryUtility.getMedian(result, sortedList);
        }
        return result;
    }

    @NotNull
    private List<IOTSummaryResponse> findNonMedianAggregation(
            IOTSummaryRequest isr)
    {
        GroupOperation groupOperation = Aggregation.group("deviceType")
                .count().as("count")
                .avg("readingValue").as("average")
                .max("readingValue").as("max")
                .min("readingValue").as("min")
                .addToSet("deviceType").as("deviceType");
        MatchOperation matchOperation = summaryUtility.mapRequestToMatchOperation(isr);
        SortOperation sortOperation = Aggregation.sort(Sort.by(Sort.Direction.ASC,
                "deviceType"));
        Aggregation aggregation = Aggregation.newAggregation(matchOperation,
                groupOperation, sortOperation);
        AggregationResults<IOTSummaryResponse> result = mongoTemplate.aggregate(aggregation,
                "DATA_IOT_DEVICES", IOTSummaryResponse.class);
        return result.getMappedResults();
    }

    /**
     * Created a sorted list (based of deviceType and reading value).
     * It is used later to calculate median
     *
     * @param isr request filters with startDate, endDate and deviceTypes
     * @return Sorted list of IOT objects
     */
    private List<IOTReading> getSortedDBResultForMedianCalc(IOTSummaryRequest isr)
    {
        logger.debug("Service getSortedList started");
        MatchOperation matchOperation = summaryUtility.mapRequestToMatchOperation(isr);
        SortOperation sortOperation = Aggregation.sort(Sort.by(Sort.Direction.ASC,
                "deviceType", "readingValue"));
        Aggregation aggregation = Aggregation.newAggregation(matchOperation, sortOperation);
        AggregationResults<IOTReading> result = mongoTemplate.aggregate(aggregation,
                "DATA_IOT_DEVICES", IOTReading.class);
        logger.debug("Service getSortedList result " + result);
        return result.getMappedResults();
    }

    public void deleteAll()
    {
        repository.deleteAll();
    }
}
