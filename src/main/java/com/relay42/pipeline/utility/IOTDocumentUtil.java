package com.relay42.pipeline.utility;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

/**
 * DocumentUtil that contains method for overriding default swagger behaviour
 */
public class IOTDocumentUtil {
    /**
     * Method that lets swagger only include the APIs under com.relay42 package
     * @return Docket object reference
     */
    public static Docket getSessionsDocket() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.relay42"))
                .build()
                .apiInfo(setAPIInfo());
    }

    /**
     * Method to override the header info for swagger.
     * @return ApiInfo object reference
     */
    private static ApiInfo setAPIInfo() {
        return new ApiInfo("Relay42 IOT API",
                "IOT APIs exposed via REST service with spring boot",
                "1.0",
                "To be reviewed",
                new Contact("Zeenat Rehman", "gitlab URL", "zeenat.assam@gmail.com"),
                "API License",
                "GIT URL",
                Collections.emptyList());
    }
}
