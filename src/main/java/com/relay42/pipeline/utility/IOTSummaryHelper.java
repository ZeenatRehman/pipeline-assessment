package com.relay42.pipeline.utility;

import com.relay42.pipeline.model.IOTReading;
import com.relay42.pipeline.model.request.IOTSummaryRequest;
import com.relay42.pipeline.model.response.IOTSummaryResponse;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import static java.util.stream.Collectors.groupingBy;

@Component
public class IOTSummaryHelper
{
    /**
     * Creates mathOperation depending on the filters in parameter
     * @param isr filters
     * @return filter object to be used in aggregation.
     */
    public MatchOperation mapRequestToMatchOperation(IOTSummaryRequest isr){
        Criteria whereClause = new Criteria("deviceType")
                .in(isr.getDeviceTypes())
                .andOperator(
                             Criteria.where("readingDateTime").gte(isr.getStartDateTime()),
                             Criteria.where("readingDateTime").lte(isr.getEndDateTime())
        );
        return Aggregation.match(whereClause);
    }

    /**
     * calculates median
     * @param lst target list where the median is to be stored
     * @param sortedIoTList source list from where median is to be calculated
     * @return target list with medians updated
     */
    public List<IOTSummaryResponse>
    getMedian(List<IOTSummaryResponse> lst,
                    List<IOTReading> sortedIoTList)
    {
        Map<String, Double> medianMap = sortedIoTList
                .stream()
                .collect(groupingBy(IOTReading::fetchDeviceTypeString))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                                          e -> calculateMedian(e.getValue()
                                                          .stream()
                                                          .map(IOTReading::getReadingValue)
                                                                  .collect(Collectors.toList()))));

        lst.stream()
                .forEach(response -> response
                        .setMedian(medianMap
                                .get(response.getDeviceType())));

        return lst;
    }

    /**
     * gets median using a sorted list
     * @param list sorted list
     * @return median from sorted list
     */
    public double calculateMedian(List<Double> list){
        DoubleStream sortedNumbers = list.stream()
                .mapToDouble(d -> d)
                .sorted();
        OptionalDouble median = (
                list.size() % 2 == 0 ?
                        sortedNumbers.skip((list.size() / 2) - 1)
                                .limit(2)
                                .average() :
                        sortedNumbers.skip(list.size() / 2)
                                .findFirst()
        );
        return median.orElse(Double.NaN);
    }

}
