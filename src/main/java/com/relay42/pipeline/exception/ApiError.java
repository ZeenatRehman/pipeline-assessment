package com.relay42.pipeline.exception;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;


public class ApiError {
    private HttpStatus status;
    private String message;
    private LocalDateTime currentDateTime;
    /**
     *
     * @param status HTTP status code
     * @param message Error message
     */
    public ApiError(HttpStatus status, String message)
    {
        this.status = status;
        this.message = message;
        this.currentDateTime = LocalDateTime.now();
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getCurrentDateTime() {
        return currentDateTime;
    }

    public void setCurrentDateTime(LocalDateTime currentDateTime) {
        this.currentDateTime = currentDateTime;
    }
}
