package com.relay42.pipeline.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class IOTCustomExceptionHandler {
    /**
     *
     * @param apiError Api error
     * @return ResponseEntity object reference
     */
    private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
    /**
     *
     * @param ex  actual exception
     * @return ResponseEntity object reference
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> customValidationErrorHandling(MethodArgumentNotValidException ex)
    {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
                ex.getBindingResult().getFieldError().getDefaultMessage());
        return buildResponseEntity(apiError);
    }
    /**
     *
     * @param ex2  actual exception
     * @return ResponseEntity object reference
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<?> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex2)
    {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
                ex2.getErrorCode()+" : between expected and passed parameters");
        return buildResponseEntity(apiError);
    }

    /**
     *
     * @param ex  actual exception
     * @return ResponseEntity object reference
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<?> handleMethodArgumentTypeMismatchException(HttpMessageNotReadableException ex)
    {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
                ex.getHttpInputMessage().toString());
        return buildResponseEntity(apiError);
    }
}
