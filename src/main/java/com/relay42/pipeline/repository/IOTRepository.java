package com.relay42.pipeline.repository;

import com.relay42.pipeline.model.IOTReading;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * repository implementation for MongoDB related CRUD operations
 */
public interface IOTRepository extends MongoRepository<IOTReading, String> {

}