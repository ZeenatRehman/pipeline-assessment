package com.relay42.pipeline.repository;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * An intermediary data structure to decouple the producer and consumer jobs. Producer puts data in it
 * and consumer picks from this. Their individual delays doesn't affect other process.
 */
@Component
public class DataProcessingQueue {
    /**
     * A Concurrent queue implementation to ensure that queue is threadsafe during get and put operations.
     */
    private final ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();
    private DataProcessingQueue(){
    }
    public void put(String data) {
        queue.offer(data);
    }
    public String get() {
        return queue.poll();
    }
    public boolean isEmpty()
    {
        return queue.isEmpty();
    }
}
