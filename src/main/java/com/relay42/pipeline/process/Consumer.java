package com.relay42.pipeline.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.relay42.pipeline.model.IOTReading;
import com.relay42.pipeline.repository.DataProcessingQueue;
import com.relay42.pipeline.service.IOTService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;


/**
 * The class is part of pipeline that consumes the queue entries
 * (in one or more threads as defined in application.properties file)
 * and saves the data into a non-sql database (embedded mongo DB in
 * this application)
 */
@Component
public class Consumer implements Runnable {
    private static final Logger logger = Logger.getLogger(Consumer.class);
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    DataProcessingQueue queue;
    @Autowired
    IOTService service;

    /**
     * The method that runs in parallel threads initiated by PipelineMaster class.
     */
    @Override
    public void run() {
            logger.debug("Consumer started " + Thread.currentThread().getName());
        ObjectMapper mapper = new ObjectMapper();
        while(true) {
            if(queue.isEmpty())
                continue;
            deQueueData(mapper);
        }
    }

    /**
     * Dequeue data from the in-memory concurrent queue and puts into a non-sql db.
    */
    public void deQueueData(ObjectMapper mapper)  {
        String data ="";
          // constantly keep polling from the queue
                    try{
                        data =  queue.get();
                        if(data==null || "".equals(data))
                            return;

                        IOTReading dataObject = mapper.readValue(data, IOTReading.class);
                        service.saveIOTDate(dataObject);

                    }
                    catch(HttpMessageNotReadableException hmnrex)
                    {
                        throw hmnrex;
                    }
                    catch(Exception ex){

                        logger.debug("Couldn't save new instance. Reason: " + ex.getMessage());
                    }

    }

}
