package com.relay42.pipeline.process;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

/**
 * This class is the orchestrator of the PIPELINE. This triggers the producer and consumer
 * threads in parallel (depending on configuration in application.properties file). This runs
 * once the application starts and IOTSimulationWorker starts to produce data.
 */
@Component
@Order(1)
public class PipelineMaster implements ApplicationListener<ApplicationReadyEvent> {
    private static final Logger logger = Logger.getLogger(PipelineMaster.class);
    public static final String DEFAULT_FILE =  "target/TEST.TXT";
    private final int DEFAULT_PRODUCER_THREADS=1;
    private final int DEFAULT_CONSUMER_THREADS=1;
    @Value("${pipeline.threads.producer}")
    private String producerThreadsString;
    @Value("${pipeline.threads.consumer}")
    private String consumerThreadsString;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    Producer producer;
    @Autowired
    Consumer consumer;

    /**
     *
     * triggers the pipeline after simulator starts creating data
     * @param applicationReadyEvent waits for application to start/ready
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        int producerThreadsSize = 0,consumerThreadsSize;
        try {
            producerThreadsSize = Integer.parseInt(producerThreadsString);
            consumerThreadsSize = Integer.parseInt(consumerThreadsString);
        }catch(Exception ex){
            logger.debug("Setting default values for number of threads for producer and consumer");
            producerThreadsSize = DEFAULT_PRODUCER_THREADS;
            consumerThreadsSize = DEFAULT_CONSUMER_THREADS;
        }
        for(int i=0;i<producerThreadsSize;i++)
            taskExecutor.execute(producer);
        for(int i=0;i<consumerThreadsSize;i++)
            taskExecutor.execute(consumer);
    }
}
