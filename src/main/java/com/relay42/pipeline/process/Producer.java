package com.relay42.pipeline.process;

import com.relay42.pipeline.repository.DataProcessingQueue;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;

@Component
public class Producer implements Runnable {
    private static final Logger logger = Logger.getLogger(Producer.class);

    @Autowired
    DataProcessingQueue queue;

    /**
     * The method that runs in parallel threads initiated by PipelineMaster class.
     */
    @Override
    public void run() {
        try {
            logger.debug("Producer started " + Thread.currentThread().getName());
            getStreamData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * The stream data is being fetched from a file in this application.
     * This method should be overloaded if data has to be fetched via a stream API.
     * @throws IOException exception is file can not be accessed
     */
    private void getStreamData()  throws IOException {
        File file = new File(PipelineMaster.DEFAULT_FILE);
        InputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isr);
        String data ;
        while(true) {
            while ((data = br.readLine()) != null) {
                queueData(data);
            }
        }
    }
    /**
     * this method queues the data to a non blocking queue
     * @param iotData json string that data represent data for a specific IOT device
     */
    private void queueData(String iotData){
         queue.put(iotData);
    }
}
