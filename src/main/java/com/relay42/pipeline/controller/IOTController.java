package com.relay42.pipeline.controller;

import com.relay42.pipeline.model.request.IOTSummaryRequest;
import com.relay42.pipeline.model.response.IOTSummaryResponse;
import com.relay42.pipeline.service.IOTService;

import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


/**
 * Controller for rest APIs to interact
 * with IOT devices aggregate summary
 */

@Api
@RestController
@RequestMapping("/iot")
public class IOTController {
    private static final Logger logger = Logger.getLogger(IOTController.class);
    private final IOTService service;

    @Autowired
    public IOTController(IOTService service)
    {
        this.service = service;
    }

    /**
     * @param summaryRequest request json with filters for device types
     *                       and start and end date
     * @return returns the list of aggregate summary objects
     */
    @PostMapping("/summary")
    public ResponseEntity<List<IOTSummaryResponse>> findSummary
    (@Valid @RequestBody IOTSummaryRequest summaryRequest) {
        logger.debug("find aggregate summary : Start");
        List<IOTSummaryResponse> summary = service
                .findAggregation(summaryRequest);
        logger.debug("find aggregate summary  : summary created");
        return new ResponseEntity<>(summary, HttpStatus.OK);
    }
}