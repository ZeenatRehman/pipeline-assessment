package com.relay42.pipeline.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


/**
 * Security class to override the default security setup by springboot.
 */
@EnableWebSecurity
public class IOTSecurityAdapter extends WebSecurityConfigurerAdapter
{
    @Value("${pipeline.ldap.url}")
    private String ldapUrl;

    /**
     * Configures the authentication from ldap.
     * @param auth AuthenticationManagerBuilder object reference
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.ldapAuthentication()
                .userDnPatterns("uid={0},ou=people")
                .groupSearchBase("ou=roles")
                .groupSearchFilter("member={0}")
                .contextSource()
                .url(ldapUrl)
                .and()
                .passwordCompare()
                .passwordEncoder(new BCryptPasswordEncoder())
                .passwordAttribute("userPassword");
    }

    /**
     * Configures the authorization from ldap. In this application it only decides the type of
     * authentication. i.e httpBasic
     * @param security HttpSecurity object
     * @throws Exception Exception
     */
    protected void configure(HttpSecurity security) throws Exception {
                 security
                         .csrf().disable()
                         .authorizeRequests()
                         .antMatchers(HttpMethod.POST).hasRole("ADMIN")
                         .antMatchers(HttpMethod.GET).hasAnyRole("USER", "ADMIN")
                         .and()
                         .httpBasic();

    }
}

