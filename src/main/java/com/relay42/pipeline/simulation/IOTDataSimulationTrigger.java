package com.relay42.pipeline.simulation;

import com.relay42.pipeline.process.PipelineMaster;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;

import static com.relay42.pipeline.simulation.IOTSimulationDefaultConstants.*;

/**
 * After the application starts, this class starts triggers creation of IOT sample JSON data
 */
@Component
@Order(0)
public class IOTDataSimulationTrigger
        implements ApplicationListener<ApplicationReadyEvent>
{


    private static final Logger logger = Logger.getLogger(IOTDataSimulationTrigger.class);
    @Value("${pipeline.devices.foreachtype}")
    private String devicePerTypeString;
    @Value("${pipeline.threadpool}")
    private String poolSizeString;
    @Value("${pipeline.threads.simulator}")
    private String simulatorThreadsString;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    private MongoTemplate template;

    /**
     * triggers the IOT data simulation after application starts
     *
     * @param applicationReadyEvent waits for application to start/ready
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent)
    {
        createDateFieldIndex(); // create index on dateField in embedded Mongo DB
        logger.debug("truncate the temporary file for every the application starts");
        truncateTempFile();
        initializePoolAndStart();
    }

    private void startThreads(int simulatorThreads, int noOfDevicesPerType)
    {
        for (int i = 0; i < simulatorThreads; i++) {
            IOTDataSimulationWorker worker = new IOTDataSimulationWorker
                    (PipelineMaster.DEFAULT_FILE, noOfDevicesPerType);
            taskExecutor.execute(worker);
        }
    }

    private void initializePoolAndStart()
    {
        int poolSize = 0,
                simulatorThreads = 0,
                noOfDevicesPerType = 0;
        try {

            poolSize = Integer.parseInt(poolSizeString);
            simulatorThreads = Integer.parseInt(simulatorThreadsString);
            noOfDevicesPerType = Integer.parseInt(devicePerTypeString);
        } catch (Exception ex) {
            logger.debug("Setting default values for pool-size");
            poolSize = DEFAULT_POOL_SIZE;
            simulatorThreads = DEFAULT_SIMULATOR_THREADS;
            noOfDevicesPerType = DEFAULT_DEVICES_PER_TYPE;
        }
        taskExecutor.setCorePoolSize(poolSize);
        startThreads(simulatorThreads, noOfDevicesPerType);
    }

    private void truncateTempFile()
    {
        try (FileOutputStream fileOutputStream =
                     new FileOutputStream(PipelineMaster.DEFAULT_FILE)) {

        } catch (IOException e) {
            logger.debug("Could not open file :" + PipelineMaster.DEFAULT_FILE);
        }
    }

    /**
     * Creates index on date field in embedded mongoDB.
     */
    private void createDateFieldIndex()
    {
        String IOT_COLLECTION_NAME = "DATA_IOT_DEVICES";
        template.indexOps(IOT_COLLECTION_NAME)
                .ensureIndex(
                        new Index().on(IOT_DATE_FIELD, Sort.Direction.ASC)
                );
    }
}
