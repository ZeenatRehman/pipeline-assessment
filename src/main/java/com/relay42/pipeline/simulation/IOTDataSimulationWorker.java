package com.relay42.pipeline.simulation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.relay42.pipeline.model.IOTReading;
import com.relay42.pipeline.model.IOTTypeEnum;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This worker class puts data into a file.
 * The producer will later starts to stream the data from this file.
 */

@Component
public class IOTDataSimulationWorker implements Runnable
{
    private static final Logger logger = Logger.getLogger(IOTDataSimulationWorker.class);
    private final int DEFAULT_DELAY = 1000;
    private String filePath;
    private IOTDataSimulationFactory factory = IOTDataSimulationFactory.getFactory();
    private int noOfDevicesPerType;

    public IOTDataSimulationWorker()
    {

    }

    public IOTDataSimulationWorker(String filePath, int noOfDevicesPerType)
    {
        this.filePath = filePath;
        this.noOfDevicesPerType = noOfDevicesPerType;
    }

    /**
     * method that runs in thread to simulate IOT json data
     */
    @Override
    public void run()
    {
        ObjectMapper mapper = new ObjectMapper();
        while (true) {
            try {
                generateData(filePath, mapper);
                Thread.sleep(DEFAULT_DELAY);
            } catch (InterruptedException | IOException e) {
                logger.debug("Simulator Interrupted and shut down" + e);
            }
        }
    }

    /**
     * Generates IOT objects using IOTEnumTypes and IOTDataSimulationFactory.
     *
     * @param statsFilePath file path
     * @param mapper        jackson object mapper to serialize / deserialize IOT objects
     */
    private void generateData(String statsFilePath, ObjectMapper mapper) throws IOException
    {
        String json = "";
        try (PrintWriter output = new PrintWriter(new FileWriter(statsFilePath, true))) {
            for (IOTTypeEnum type : IOTTypeEnum.values()) {

                for (int i = 0; i < noOfDevicesPerType; i++) {
                    IOTReading data = factory.getNewIOTDataInstance(type,
                            type.toString() + i);
                    if (data == null) {
                        logger.error(type + " device is not registered in factory");
                        continue;
                    }
                    json = mapper.writeValueAsString(data);
                    output.printf("%s\n", json);
                }
            }
        }

    }
}
