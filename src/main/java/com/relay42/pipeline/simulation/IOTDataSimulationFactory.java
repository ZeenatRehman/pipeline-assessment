package com.relay42.pipeline.simulation;

import com.relay42.pipeline.model.*;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A singleton factory to generate IOT objects of a specific implementation
 */
public class IOTDataSimulationFactory {
    /**
     * Registry to register all the new IOT devices in the factory
     */
    private static final Map<IOTTypeEnum, Class<? extends IOTReading>> registry = new ConcurrentHashMap<>();
    private static final Logger logger = Logger.getLogger(IOTDataSimulationFactory.class);
    private IOTDataSimulationFactory(){
    }
    private static class IOTDataSimulationFactoryInner{
        private static final IOTDataSimulationFactory factory = new IOTDataSimulationFactory();
    }
    static IOTDataSimulationFactory getFactory(){
        return IOTDataSimulationFactoryInner.factory;
    }

    /**
     * Creates a specific IOT device instance if its found in registration. The below design pattern
     * avoids all the if else logic in the factory. New IOT devices can be added without any changes
     * to factory code
      * @param type
     * @return returns an IOT instance
     */
    public IOTReading getNewIOTDataInstance(IOTTypeEnum type,String deviceID){
        IOTReading device = null;
        switch(type) {
            case CAR_FUEL_READER:
                device = new CarFuelIndicatorReading(deviceID);
                break;
            case HEART_RATE_MONITOR:
                device = new HeartRateMonitorReading(deviceID);
                break;
            case THERMOSTAT:
                device = new ThermostatReading(deviceID);
                break;
            default:
                logger.debug("This type is not supported yet");
        }
        return device;
    }
}
