package com.relay42.pipeline.simulation;

public class IOTSimulationDefaultConstants
{
    public static final int DEFAULT_SIMULATOR_THREADS=1;
    public static final String IOT_DATE_FIELD = "readingDateTime";
    public static final int DEFAULT_DEVICES_PER_TYPE=1;
    public static final int DEFAULT_POOL_SIZE = 10;
}
