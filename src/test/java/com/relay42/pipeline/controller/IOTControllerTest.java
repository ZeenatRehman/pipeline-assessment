package com.relay42.pipeline.controller;

import com.relay42.pipeline.model.request.IOTSummaryRequest;
import com.relay42.pipeline.model.response.IOTSummaryResponse;
import com.relay42.pipeline.service.IOTService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(IOTController.class)
public class IOTControllerTest
{
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private IOTService service;
    private List<IOTSummaryResponse> summaryResponseList;

    @BeforeEach
    void setUp()
    {
        summaryResponseList = Arrays.asList(new IOTSummaryResponseBuilder().build());
        //assumeTrue(mockMvc != null);
    }

    @AfterEach
    void tearDown()
    {
    }

    @WithMockUser(username = "zrehman", roles = "ADMIN")
    @Test
    void findSummary() throws Exception
    {
        when(service.findAggregation(any(IOTSummaryRequest.class)))
                .thenReturn(summaryResponseList);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .post("/iot/summary")
                .content("{\n" +
                        "  \"deviceTypes\": [\n" +
                        "    \"CAR_FUEL_READER\", \"THERMOSTAT\"\n" +
                        "  ],\n" +
                        "  \"endDateTime\": \"2021-03-24T17:22:04\",\n" +
                        "  \"startDateTime\": \"2021-03-23T17:22:06\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .accept(MediaType.APPLICATION_JSON)
        )
                .andReturn();
        assertAll(
                () -> assertNotNull(result.getResponse()),
                () -> assertEquals(200, result.getResponse().getStatus())
        );

    }
}

class IOTSummaryResponseBuilder
{
    IOTSummaryResponse build()
    {
        IOTSummaryResponse response = new IOTSummaryResponse();
        response.setMedian(22.291027491597237);
        response.setAverage(22.291027491597237);
        response.setCount(2);
        response.setDeviceType("CAR_FUEL_READER");
        response.setMax(29.2124520931527);
        response.setMin(15.369602890041733);
        return response;
    }
}