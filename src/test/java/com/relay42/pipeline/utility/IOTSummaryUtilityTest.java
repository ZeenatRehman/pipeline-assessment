package com.relay42.pipeline.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.relay42.pipeline.model.IOTReading;
import com.relay42.pipeline.model.IOTTypeEnum;
import com.relay42.pipeline.model.response.IOTSummaryResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class IOTSummaryUtilityTest
{
    List<IOTReading> list;

    IOTSummaryHelper utility;
    List<Double> readingList;
    @BeforeEach
    void setUp() throws IOException
    {
        utility = new IOTSummaryHelper();
        list = new IOTReadingListBuilder().build();
        readingList = list.stream().map(e -> e.getReadingValue()).collect(Collectors.toList());
    }

    @Test
    void calculateMedian()
    {
        double median = utility.calculateMedian(readingList);
        assertEquals(22.836587811947595, median);
    }

    @Test
    void getMedian() throws IOException
    {
        List<IOTSummaryResponse> mappedResults = new ArrayList<>();
        List<IOTSummaryResponse> results =
                new SummaryResponseBuilder().build();
        List<IOTReading> sortedList = new IOTReadingListBuilder().build();
        List<IOTSummaryResponse> responseWithMedian =
                utility.getMedian(results, sortedList);
        assertAll(
                () -> assertEquals(19.31079036192166,responseWithMedian.get(0).getMedian()),
                () -> assertEquals("CAR_FUEL_READER",responseWithMedian.get(0).getDeviceType()),
                () -> assertEquals(70.23625979947089,responseWithMedian.get(1).getMedian()),
                () -> assertEquals("THERMOSTAT",responseWithMedian.get(1).getDeviceType()),
                () -> assertEquals(32.65258164625671,responseWithMedian.get(2).getMedian()),
                () -> assertEquals("HEART_RATE_MONITOR",responseWithMedian.get(2).getDeviceType())
                );


    }
}
class SummaryResponseBuilder
{
    List<IOTSummaryResponse> build()
    {
        IOTSummaryResponse carReading = new IOTSummaryResponse();
        carReading.setDeviceType(IOTTypeEnum.CAR_FUEL_READER.toString());
        IOTSummaryResponse thermostatReading = new IOTSummaryResponse();
        thermostatReading.setDeviceType(IOTTypeEnum.THERMOSTAT.toString());
        IOTSummaryResponse heartReading = new IOTSummaryResponse();
        heartReading.setDeviceType(IOTTypeEnum.HEART_RATE_MONITOR.toString());

        List<IOTSummaryResponse> summaryList = Arrays.asList(carReading, thermostatReading,heartReading );
        return summaryList;
    }

}
class IOTReadingListBuilder
{
    public List<IOTReading> build() throws IOException
    {
        List<IOTReading> list = new ArrayList<>();
        File file = new File("src/test/resources/ReadingsFile.txt");
        try (InputStream fis = new FileInputStream(file);
             InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr);
        ) {
            String data;
            while ((data = br.readLine()) != null) {
                ObjectMapper mapper = new ObjectMapper();
                IOTReading reading = mapper.readValue(data, IOTReading.class);
                list.add(reading);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return list;
    }
}