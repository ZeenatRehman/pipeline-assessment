package com.relay42.pipeline.process;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.relay42.pipeline.model.CarFuelIndicatorReading;
import com.relay42.pipeline.model.IOTTypeEnum;
import com.relay42.pipeline.model.IOTUnitEnum;
import com.relay42.pipeline.repository.DataProcessingQueue;
import com.relay42.pipeline.service.IOTService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConsumerTest
{
    @Mock
    DataProcessingQueue queue;
    @Mock
    IOTService service;
    @Mock
    ObjectMapper mapper;
    @InjectMocks
    Consumer consumer;
    CarFuelIndicatorReading reading;

    @Test
    void run() throws JsonProcessingException
    {
        ArgumentCaptor<CarFuelIndicatorReading> iotReadingArgumentCaptor = ArgumentCaptor.forClass(CarFuelIndicatorReading.class);
        when(queue.get()).thenReturn(new QueueProducer().deQueue());
        consumer.deQueueData(new ObjectMapper());
        verify(service, times(1)).saveIOTDate(iotReadingArgumentCaptor.capture());
        CarFuelIndicatorReading actualReading = iotReadingArgumentCaptor.getValue();
        assertAll(
                () -> assertEquals(IOTTypeEnum.CAR_FUEL_READER, actualReading.getDeviceType()),
                () -> assertEquals(IOTUnitEnum.KMPL, actualReading.getReadingUnit()),
                () -> assertEquals(24.326425767764725, actualReading.getReadingValue()),
                () -> assertEquals("CAR_FUEL_READER0", actualReading.getDeviceId())
        );

    }
}

class QueueProducer
{
    String deQueue()
    {

        return "{\"deviceType\":\"CAR_FUEL_READER\",\"readingValue\":24.326425767764725,\"readingUnit\":\"KMPL\",\"readingDateTime\":\"2021-03-25 10:01:34\",\"deviceId\":\"CAR_FUEL_READER0\",\"deviceTypeString\":\"CAR_FUEL_READER\"}";

    }
}

