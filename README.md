## Application architecture

![Alt text](Pipeline.png?raw=true "The application architecture")

## Requirement coverage
* Simulates reading data for three IOT type of devices with 2 unique devices for each type. Configurable to simulate any number of devices
  per device type with property in application.properties. Default setting: pipeline.devices.foreachtype=2
* Extendible and scalable to add more types of devices. IOTReading is the main abstract class which new device modal 
  will have to extend from. A little update to the factory and that is it. 
* Fast: Uses a no SQL DB for storing IOT readings. Configurable number of threads for producer,
  consumer and simulated data creation.
* Self contained. Uses embedded ldap server for security and embedded mongo db.
* Security: Web services are secured using ldap authentication and authorization. Once a successful authentication is
  done, then the authorization is also checked for the user.  

## Application setup and high level approach
1. Applcation starts
2. IOTDataSimulationTrigger.java: Starts after the application starts. Triggers the simulation of IOT reading data by spawning worker threads.
   * IOTDataSimulationWorker.java: Worker threads, gets the device types from enum. And asks the factory to return a reading object to write into file
   * IOTDataSimulationFactory.java: Create and returns the instance of specific IOT device type reading. Worker thread writes into a file every one second 
3. PipelineMaster.java: Starts after the application starts.  Triggers the pipeline by triggering producer and consumer threads. 
   * Producer.java: Reads the stream from a file as stream. Source of simulated stream is file in this application. 
   * DataProcessingQueue.java: All the producer threads put the data in this threadsafe queue.
   * Consumer.java: Reads the queued data and saved into a no SQL database.

4. IOTController.java exposes a post method (/iot/summary). Sample request: {"startDateTime":"2021-03-23T21:24:00","endDateTime":"2021-03-23T22:11:56","deviceTypeasdsds":["CAR_FUEL_READER1","THERMOSTAT"]} 
   and returns various aggregated data grouped by each device type.

## How to run and test
1. Application can be started with 
   * mvn spring-boot:run
2. Rest endpoint is http://localhost:8080/iot/summary.
   * Sample request:  {"startDateTime":"2021-03-23T21:24:00","endDateTime":"2021-03-23T22:11:56","deviceTypeasdsds":     ["CAR_FUEL_READER1","THERMOSTAT"]} 
   * Detailed swagger documentation: http://localhost:8080/swagger-ui.html      
   * The readings can be seen at target/TEST.TXT and startDateTime and endDateTime
     can be referred from here for request json.
3. Basic Authentication 
   * user: ben
   * password: benspassword  
   
## Technology stack
1. Springboot
2. Java 8
3. Embedded Mongo Db server
4. Log4j
5. Embedded unboundid ldap server 
6. Swagger
7. jackson                  
